
-- SUMMARY --

When logged in as a Drafter:
The user can create new articles, but the articles are put into an unpublished state.
When the unpublished node is saved, a new workflow node is created and can be accessed by clicking on the workflow tab where the edit tab used to be.
Drafters do not have access to the node edit pages on original nodes; only workflow nodes.
The drafter can then change the status of the workflow node (statuses include: draft, submitted, published).
Drafters can only change the status to submitted.
Upon saving the node with a submitted status, an email is sent to the approver and any updates are saved in the workflow log for the original node.
Existing nodes can begin the workflow process as well by clicking on the workflow tab and then clicking on a button "Begin Workflow".
Upon clicking "Begin Workflow" a new workflow node is created using the original nodes field contents.
The drafter can then make their changes, provide a note in the workflow log, and change the status to submitted.
When logged in as an Approver:
The approver has access to a workflow list (a list of nodes that are in workflow).
He/she can view the workflow node and proofread the content and change the status either to draft or published.
If they change it to draft, an email is generated to the editor along with any comments from the workflow log.
If they approve it the workflow node body and field contents are merged with the original node and the workflow node is deleted.
All of the changes are saved in the workflow log and are stored with the original node id.

For a full description of the module, visit the project page:
  http://drupal.org/project/simple_workflow

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/simple_workflow


-- DEFAULTS --

This module will install two new roles drafter and approver.  These roles have all the permissions that you will need to get started.  
You can delete these if you want to customize the roles.  
The important portion is the permissions on the user admin side.

The default status options are draft, submitted and approved.  These can be removed as well and replaced with your own custom options.

There is only one default group.  This group when assigned to users will give the ability to perform workflow on all node types that are 
approved for workflow.


-- REQUIREMENTS --

This module does integrate with views, if you wish to generate views with workflow data then you will need to install the views module.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer Simple Workflow 

    This is permission for an admin user to change the groups and status options.

  - Perform Workflow

    This is the bare bones permission that will give users access to the workflow tab on their group's nodes.  
	It also handles all the node access needed. YOU DO NOT NEED TO GRANT NODE ADD PERMISSIONS for this to work.

  - Assign Groups
	
	This permission will allow a user the ability to change nodes groups.  If this is used in conjunction with the administer users (user module) 
	then users will be able assign groups to other users.
	
* The rest of configuration can be found at Configuration » Content Authoring » Simple Workflow
	
	- Select Node Types that will be in need of workflow
	- Add or change existing Status Options
	- Add or change existing groups
	- The default group, if assigned to a user, will have access to all nodes workflows.
	
* Once you have configured everything on the admin interface you can create user or modify existing users.  Be sure to assign them a role and groups.



-- CONTACT --

Current maintainers:
* Ben Routson (bentekwork) - http://drupal.org/user/698096

