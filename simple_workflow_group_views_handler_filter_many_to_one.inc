<?php
class simple_workflow_group_views_handler_filter_many_to_one extends views_handler_filter_many_to_one {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = db_select('simple_workflow_groups', 'g')
						 		->fields('g', array('id', 'group_name'))
					    		->execute()
								->fetchAllKeyed();
  }
}
