<?php

function simple_workflow_page($node) {
  if (!empty($node->workflow_node)) {
    drupal_goto('node/' . $node->workflow_node . '/edit');
  }
  else {
    $form = drupal_get_form('simple_workflow_begin_form', $node);
  }
  return $form;
}
function simple_workflow_original_page($node) {
  drupal_goto('node/' . $node->original_node);

}

function simple_workflow_begin_form($form, &$form_state, $node) {
  $form = array();
  $form['nid'] = array(
		'#title' => 'nid',
		'#type' => 'hidden',
		'#default_value' => $node->nid,
	);
  $form['verify'] = array(
		'#markup' => '<div>Do you wish to begin workflow on this ' . $node->type . '</div>',
	);
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Yes',
		'#suffix' => '<a href = "/node/' . $node->nid . '">Cancel</a>',
	);
  return $form;
}
function simple_workflow_begin_form_submit($form, &$form_state) {

  $node = node_load($form_state['values']['nid']);
  $workflow_node = simple_workflow_build_workflow_node($node);
  drupal_goto('node/' . $workflow_node->nid . '/edit');

}

//Confirmation of Publish form when status is changed to publish.
function simple_workflow_publish_form($form, &$form_state, $node, $workflow_node) {
  $form = array();
  $form['node_arg'] = array(
		'#type' => 'hidden',
		'#default_value' => $node->nid,
	);
  $form['node_vid'] = array(
		'#type' => 'hidden',
		'#default_value' => $node->vid,
	);
  $form['workflow_node_arg'] = array(
		'#type' => 'hidden',
		'#default_value' => $workflow_node->nid,
	);
  $form['workflow_node_vid'] = array(
		'#type' => 'hidden',
		'#default_value' => $workflow_node->vid,
	);
  $form['confirm'] = array(
		'#markup' => '<div>Are you sure that you wish to replace the production content with the workflow content?</div>'
	);
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Confirm',
		'#suffix' => l('Cancel', 'node/' . $node->nid . '/workflow'),
	);
  return $form;
}

//Confirmation Submit Handler
function simple_workflow_publish_form_submit($form, &$form_state) {
  //replace original node with workflow node contents
  global $user;
  $workflow_node = node_load($form_state['values']['workflow_node_arg'], $form_state['values']['workflow_node_vid'], $reset = TRUE);
  $node = node_load($form_state['values']['node_arg'], $form_state['values']['node_vid'], $reset = TRUE);

  //set the node author on the workflow node back to the original author
  $workflow_node->uid = $node->uid;
  //loop through workflow node and add fields
  foreach ($workflow_node as $field_name => $fields) {
    $node->$field_name = $fields;
  }
  $node->nid = $form_state['values']['node_arg'];
  $node->vid = $form_state['values']['node_vid'];
  $node->status = 1;
  node_save($node);

  //send out published email notifications
  simple_workflow_send_email_notifications($user->uid, $workflow_node->nid, $node->nid, simple_workflow_get_publish_status_id(), $comment = 'Workflow Status Changed to: Published');

  //delete the previous node.
  $delete = node_delete($workflow_node->nid);
  //add new comment
  $comment = simple_workflow_workflow_comment($form_state['values']['node_arg'], 'Workflow Status Changed to: Published');

  //redirect user to the newly updated node!
  drupal_goto('node/' . $form_state['values']['node_arg']);

}

function simple_workflow_list_page() {
  //This will return a list of workflow nodes that are in this user's group
  global $user;
  $nodes = db_select('simple_workflow_node_map', 'w');
  $nodes->join('node', 'n', 'w.nid = n.nid');
  $nodes->fields('n', array('nid', 'title'));
  $nodes->fields('w', array('new_nid', 'status_id'));
  $result = $nodes->execute();
  while ($record = $result->fetchAssoc()) {
    $record['edit'] = '<a href="/node/' . $record['nid'] . '/workflow">Edit</a>';
    $rows[] = $record;
    $header = array_keys($record);
  }
  return theme('table', array(
	  'header' => $header,
	  'rows' => $rows,
	));
}
