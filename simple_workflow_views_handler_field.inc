<?php


class simple_workflow_views_handler_field extends views_handler_field {

  function render($values) {
    $options = db_select('simple_workflow_status_options', 's')
						->fields('s', array('id', 'title'))
					    ->execute()
						->fetchAllKeyed();
    return $options[$values->simple_workflow_node_map_status_id];

  }

}
