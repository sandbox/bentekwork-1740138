<?php
class simple_workflow_views_handler_filter_many_to_one extends views_handler_filter_many_to_one {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = db_select('simple_workflow_status_options', 's')
						 		->fields('s', array('id', 'title'))
					    		->execute()
								->fetchAllKeyed();
  }
}
