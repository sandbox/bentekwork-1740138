<?php

function simple_workflow_admin_page() {
  $content = '
	<ul class="admin-list"><li class="leaf"><a href="/admin/config/content/simple-workflow/node-types">Configure Node Types</a><div class="description">Step 1:  You must select what node types you will perform workflow on.</div></li><li class="leaf"><a href="/admin/config/content/simple-workflow/status-options">Configure Status Options</a><div class="description">Step 2:  You must decide on what steps (aka statuses) the workflow will go through.</div></li><li class="leaf"><a href="/admin/config/content/simple-workflow/groups">Groups</a><div class="description">Step 3:  Optionally decide if there will be different groups that can control workflow on their own nodes.</div></li></ul>
  ';
  return $content;
}

function simple_workflow_admin_node_types_options_form($form, &$form_state) {
  $form = array();
  $names = node_type_get_names();

  $form['node_types'] = array(
		'#title' => 'Node Types',
		'#type' => 'checkboxes',
		'#options' => $names,
		'#default_value' => variable_get('simple_workflow_nodetypes', array()),	
	);
  $form['submit'] = array(
  		'#type' => 'submit',
		'#value' => 'Save Configuration'
  );
  return $form;
}

function simple_workflow_admin_node_types_options_form_submit($form, &$form_state) {
  foreach ($form_state['values']['node_types'] as $type) {
    if ($type != '0') {
      $types[] = $type;
    }
  }
  $save = variable_set('simple_workflow_nodetypes', $types);
  drupal_set_message('Configuration Saved');
}

function simple_workflow_admin_status_options_form($form_state) {
  
  $form = array();
  $status_list = db_select('simple_workflow_status_options', 's')
						->fields('s')
						->orderby('delta', 'ASC')
					    ->execute()
						->fetchAll(PDO::FETCH_ASSOC);
  $rows = array();
  $form['status_options']['#tree'] = TRUE;

  foreach ($status_list as $status) {
	$form['status_options'][$status['id']] = array(
		 // We'll use a form element of type '#markup' to display the item name.
	      'id' => array(
	        '#markup' => check_plain($status['id']),
	      ),
	      'title' => array(
	        '#markup' => check_plain($status['title']),
	      ),
	      'assign_roles' => array(
	        '#markup' => (is_array(unserialize($status['assign_roles'])) ? implode(',', unserialize($status['assign_roles'])) : '' ),
		  ),
	      'notify_roles' => array(
	        '#markup' => (is_array(unserialize($status['notify_roles'])) ? implode(',', unserialize($status['notify_roles'])) : '' ),
	      ),
	      'draft' => array(
	        '#markup' => check_plain($status['draft']),
	      ),
	      'publishes' => array(
	        '#markup' => check_plain($status['publishes']),
	      ),
	      'options' => array(
	        '#markup' => '<a href="/admin/config/content/simple-workflow/status-options/editstatus/'.$status['id'].'">Edit</a> | <a href="/admin/config/content/simple-workflow/status-options/deletestatus/'.$status['id'].'">Delete</a>',
	      ),
	      'weight' => array(
	        '#type' => 'weight',
	        '#title' => t('Weight'),
	        '#default_value' => $status['delta'],
	        '#delta' => 10,
	        '#title-display' => 'invisible',
	      ),
	);
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array( '#type' => 'submit', '#value' => t('Save Changes'));
  $form['actions']['markup'] = array(
		'#markup' => '<a class="button" href="/admin/config/content/simple-workflow/status-options/addstatus">Add Status Option</a>',
	);
  return $form;
}

/**
 * Submit callback for the tabledrag_example_simple_form form
 *
 * Updates the 'weight' column for each element in our table, taking into
 * account that item's new order after the drag and drop actions have been
 * performed.
 */
function simple_workflow_admin_status_options_form_submit($form, &$form_state) {
  // Because the form elements were keyed with the item ids from the database,
  // we can simply iterate through the submitted values.
  foreach ($form_state['values']['status_options'] as $id => $item) {
    db_query(
      "UPDATE {simple_workflow_status_options} SET delta = :weight WHERE id = :id",
      array(':weight' => $item['weight'], ':id' => $id)
    );
  }
}

function theme_simple_workflow_admin_status_options_form($variables) {
  $form = $variables['form'];
  $rows = array();

  foreach (element_children($form['status_options']) as $id) {

    $form['status_options'][$id]['weight']['#attributes']['class'] = array('status-option-weight');

    $rows[] = array(
      'data' => array(
        // Add our 'name' column
        drupal_render($form['status_options'][$id]['id']),
        drupal_render($form['status_options'][$id]['title']),
        drupal_render($form['status_options'][$id]['assign_roles']),
        drupal_render($form['status_options'][$id]['notify_roles']),
        drupal_render($form['status_options'][$id]['draft']),
        drupal_render($form['status_options'][$id]['publishes']),
        drupal_render($form['status_options'][$id]['options']),
        drupal_render($form['status_options'][$id]['weight']),

      ),

      'class' => array('draggable'),
    );
  }
  $header = array(t('Id'), t('Title'), t('Assign Roles'), t('Notify Roles'), t('Draft'), t('Publishes'), t('Options'), t('Weight'));
  $table_id = 'status-option-table';

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id)));
  $output .= drupal_render_children($form);
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'status-option-weight');

  return $output;
}

function simple_workflow_admin_status_form_template(){

  $workflow_users = db_select('users', 'u');
  $workflow_users->join('users_roles', 'r', 'u.uid = r.uid');
  $workflow_users->join('role_permission', 'p', 'r.rid = p.rid');
  $workflow_users->fields('u', array('uid', 'name'));
  $workflow_users->fields('r', array('rid'));
  $workflow_users->fields('p', array('permission'));
  $workflow_users->condition('p.permission', 'perform workflow', '=');
  $result = $workflow_users->execute();
  while ($record = $result->fetchAssoc()) {
    $user_options[$record['uid']] = $record['name'];
  }
  $form = array();
  $form['status'] = array(
		'#title' => 'Status Title',
		'#type' => 'textfield',
		'#required' => true
	);
  $form['assign_role'] = array(
		'#title' => 'Assign Roles',
		'#type' => 'select',
		'#multiple' => TRUE,
		'#options' => user_roles(TRUE),
		'#description' => 'These are roles that assign this status to a workflow',
	);
  $form['email_role'] = array(
		'#title' => 'Notify Roles',
		'#type' => 'select',
		'#multiple' => TRUE,
		'#options' => user_roles(TRUE),
		'#description' => 'These are roles that are notified when this status is assigned to a node',
	);
  $form['assign_user'] = array(
		'#title' => 'Assign Users',
		'#type' => 'select',
		'#multiple' => TRUE,
		'#options' => $user_options,
		'#description' => 'These are users that assign this status to a workflow.  The users in this list have the "perform workflow" permission',
	);
  $form['email_user'] = array(
		'#title' => 'Notify Users',
		'#type' => 'select',
		'#multiple' => TRUE,
		'#options' => $user_options,
		'#description' => 'These are users that assign this status to a workflow.  The users in this list have the "perform workflow" permission.  Users must also belong to the nodes group and have email notifications turned on.',
	);
  $form['publishes'] = array(
		'#type' => 'checkbox',
		'#title' => 'This status will publish the node when assigned',
		'#options' => array(0 => 'This status will publish the node when assigned', 1 => 'This status will publish the node when assigned' ),
		'#description' => 'If checked, this status will publish the node when assigned to a workflow.',
	);
  $form['draft'] = array(
		'#type' => 'checkbox',
		'#title' => 'This status will only notify the workflow originating user',
		'#options' => array(0 => 'This status will only notify the workflow originating user', 1 => 'This status will only notify the workflow originating user' ),
		'#description' => 'If checked, this status will only notify the originating user and not the whole group.',
	);
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Configuration',
		'#suffix' => '<a href = "/admin/config/content/simple-workflow/status-options">Cancel</a> | <a href = "/admin/config/content/simple-workflow/status-options">Back to Status Options</a>',
	);
  return $form;
}

function simple_workflow_admin_addstatus($form, &$form_state) {

  $form = simple_workflow_admin_status_form_template();
  return $form;
}

function simple_workflow_admin_addstatus_submit($form, &$form_state) {
  foreach ($form_state['values']['email_role'] as $e_role) {
    $e_roles[$e_role] = user_role_load($e_role)->name;
  }
  foreach ($form_state['values']['assign_role'] as $a_role) {
    $a_roles[$a_role] = user_role_load($a_role)->name;
  }
  foreach ($form_state['values']['assign_user'] as $a_user) {
    $a_users[$a_user] = user_load($a_user)->name;
  }
  foreach ($form_state['values']['email_user'] as $e_user) {
    $e_users[$e_user] = user_load($e_user)->name;
  }
  
  $delta_q = db_select('simple_workflow_status_options', 'o')
			 			->fields('o', array('delta'))
						->orderby('delta', 'DESC')
						->range(0,1)
						->execute()
						->fetchColumn();
  $delta = $delta_q + 1;
  $record = db_insert('simple_workflow_status_options')
						->fields(array(
						  'title' => $form_state['values']['status'],
						  'assign_roles' => serialize($a_roles),
						  'notify_roles' => serialize($e_roles),
						  'assign_users' => serialize($a_users),
						  'notify_users' => serialize($e_users),
						  'publishes' => $form_state['values']['publishes'],
						  'draft' => $form_state['values']['draft'],
						  'delta' => $delta
						))
						->execute();
  drupal_set_message('Status Option Added');
  drupal_goto('admin/config/content/simple-workflow/status-options');
}
function simple_workflow_admin_editstatus($form, &$form_state, $status_id){
	$status = db_select('simple_workflow_status_options', 'o')
			  			->fields('o')
						->condition('id', $status_id, '=')
						->execute()
						->fetchObject();
    $form = simple_workflow_admin_status_form_template();
 	$form['status']['#default_value'] = $status->title;
 	$form['assign_role']['#default_value'] = (is_array(unserialize($status->assign_roles)) ? array_keys(unserialize($status->assign_roles)) : '');
 	$form['email_role']['#default_value'] = (is_array(unserialize($status->notify_roles)) ? array_keys(unserialize($status->notify_roles)) : '');
 	$form['assign_user']['#default_value'] = (is_array(unserialize($status->assign_users)) ? array_keys(unserialize($status->assign_users)) : '');
 	$form['email_user']['#default_value'] = (is_array(unserialize($status->notify_users)) ? array_keys(unserialize($status->notify_users)) : '');
 	$form['publishes']['#default_value'] = $status->publishes;
 	$form['draft']['#default_value'] = $status->draft;
	$form['#status_id'] = $status_id;
	return $form;
}

function simple_workflow_admin_editstatus_submit($form, &$form_state) {
  foreach ($form_state['values']['email_role'] as $e_role) {
    $e_roles[$e_role] = user_role_load($e_role)->name;
  }
  foreach ($form_state['values']['assign_role'] as $a_role) {
    $a_roles[$a_role] = user_role_load($a_role)->name;
  }
  foreach ($form_state['values']['assign_user'] as $a_user) {
    $a_users[$a_user] = user_load($a_user)->name;
  }
  foreach ($form_state['values']['email_user'] as $e_user) {
    $e_users[$e_user] = user_load($e_user)->name;
  }

  $record = db_update('simple_workflow_status_options')
						->fields(array(
						  'title' => $form_state['values']['status'],
						  'assign_roles' => serialize($a_roles),
						  'notify_roles' => serialize($e_roles),
						  'assign_users' => serialize($a_users),
						  'notify_users' => serialize($e_users),
						  'publishes' => $form_state['values']['publishes'],
						  'draft' => $form_state['values']['draft'],
						))
  						->condition('id', $form['#status_id'], '=')
  						->execute();
  drupal_set_message('Status Option Changed');
}

function simple_workflow_admin_removestatus($status_id) {

  $deleted = db_delete('simple_workflow_status_options')
		  ->condition('id', $status_id)
		  ->execute();
  drupal_set_message('Status deleted');
  drupal_goto('admin/config/content/simple-workflow');
}


function simple_workflow_admin_groups_form($form, &$form_state) {
  $form = array();

  $group_list = db_select('simple_workflow_groups', 's')
						->fields('s')
					    ->execute()
						->fetchAll(PDO::FETCH_ASSOC);
  $g_rows = array();
  foreach ($group_list as $group) {
    $group['options'] = '<a href="/admin/config/content/simple-workflow/groups/editgroup/' . $group['id'] . '">Edit</a> | <a href="/admin/config/content/simple-workflow/groups/deletegroup/' . $group['id'] . '">Delete</a>';
    $g_rows[] = $group;

  }
  $g_header = array(t('Id'), t('Group Name'), t('Delete'));
  $g_table = theme('table', array(
	  'header' => $g_header,
	  'rows' => $g_rows,
	));
  $form['g_markup'] = array(
		'#markup' => $g_table . '<a class="button" href="/admin/config/content/simple-workflow/groups/addgroup">Add Group</a>',
	);

  return $form;
}

function simple_workflow_admin_addgroup($form, &$form_state) {

  $form = array();
  $form['group_name'] = array(
		'#title' => 'Group Name',
		'#type' => 'textfield',
		'#required' => true
	);
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Configuration',
		'#suffix' => '<a href = "/admin/config/content/simple-workflow/groups">Cancel</a> | <a href = "/admin/config/content/simple-workflow/groups">Back to Groups</a>',
	);
  return $form;
}

function simple_workflow_admin_addgroup_submit($form, &$form_state) {

  $record = db_insert('simple_workflow_groups')
						->fields(array(
						  'group_name' => $form_state['values']['group_name'],
						))
						->execute();
  drupal_set_message('Group Added');
  drupal_goto('admin/config/content/simple-workflow/groups');
}
function simple_workflow_admin_editgroup($form, &$form_state, $group_id){

  $name = db_select('simple_workflow_groups', 'g')
		  ->fields('g', array('group_name'))
		  ->condition('id', $group_id, '=')
	      ->execute()
		  ->fetchColumn();
  $form = array();
  $form['group_name'] = array(
		'#title' => 'Group Name',
		'#type' => 'textfield',
		'#required' => true,
		'#default_value' => $name
	);
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Configuration',
		'#suffix' => '<a href = "/admin/config/content/simple-workflow/groups">Cancel</a> | <a href = "/admin/config/content/simple-workflow/groups">Back to Groups</a>',
	);
  $form['#group_id'] = $group_id;
  return $form;	
	
}
function simple_workflow_admin_editgroup_submit($form, &$form_state) {

  $record = db_update('simple_workflow_groups')
						->fields(array(
						  'group_name' => $form_state['values']['group_name'],
						))
						->condition('id', $form['#group_id'], '=')
						->execute();
  drupal_set_message('Group Saved');
}
function simple_workflow_admin_removegroup($group_id) {

  $deleted = db_delete('simple_workflow_groups')
		  ->condition('id', $group_id)
		  ->execute();
  drupal_set_message('Group deleted');
  drupal_goto('admin/config/content/simple-workflow');
}